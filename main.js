document.addEventListener('DOMContentLoaded', () => {
  const emojis = ['😀', '😂', '👍', '💻', '🎨', '🚀'];
  const body = document.querySelector('body');

  function createEmojiFlyer(emoji) {
    const emojiElement = document.createElement('div');
    emojiElement.textContent = emoji;
    emojiElement.style.position = 'absolute';
    emojiElement.style.left = `${Math.random() * 100}vw`;
    emojiElement.style.top = `${Math.random() * 100}vh`;
    emojiElement.style.fontSize = '2rem';
    body.appendChild(emojiElement);

    // Animate the emoji
    const animationDuration = Math.random() * 5 + 5;
    emojiElement.animate([
      { transform: 'translate3d(0, 0, 0)' },
      { transform: 'translate3d(-100vw, 0, 0)' }
    ], {
      duration: animationDuration * 1000,
      iterations: Infinity,
      direction: 'alternate',
      easing: 'linear'
    });
  }

  // Create multiple emojis
  emojis.forEach(createEmojiFlyer);
});
